# Wen - A Python 3 script to practice Chinese characters

Wen generates a quiz for Mandarin Chinese characters by reading a file from a
"dictionaries/" directory in the working directory. The quiz shows a character,
with or without pinyin with diacritics, and asks the user to define it.

## Creating Dictionaries

To use Wen to generate a custom test, the file must contain one translation
per line. Each line has three parts, separated by a comma (", "):

    1. The Chinese character(s) (e.g. "汉字")
    2. The Hanyu Pinyin with tone numbers (e.g. "Ha4nzi4")
    3. The definition, or definitions separated by a forward slash ("/") 
       (e.g. "Chinese character" or "Chinese chracter/written character")
