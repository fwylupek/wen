#!/usr/bin/python3

# I wrote this script to practice Mandarin Chinese characters. It looks
# in the working directory for a directory named "dictionaries/" to load
# characters, pinyin, and definitions, and generates a quiz for an exact
# match to one or more definitions.

import sys
import os
import random


# Replace tone number with diacritics (accents).
def replace_tone(pinyin):
    working_list = []

    a_set = ('ā', 'á', 'ă', 'à')
    e_set = ('ē', 'é', 'ĕ', 'è')
    i_set = ('ī', 'í', 'ĭ', 'ì')
    o_set = ('ō', 'ó', 'ŏ', 'ò')
    u_set = ('ū', 'ú', 'ŭ', 'ù')
    v_set = ('ǖ', 'ǘ', 'ǚ', 'ǜ')

    for word in pinyin:
        for num in range(4):
            word = word.replace(str('a' + str(num + 1)), a_set[num])
            word = word.replace(str('e' + str(num + 1)), e_set[num])
            word = word.replace(str('i' + str(num + 1)), i_set[num])
            word = word.replace(str('o' + str(num + 1)), o_set[num])
            word = word.replace(str('u' + str(num + 1)), u_set[num])
            word = word.replace(str('v' + str(num + 1)), v_set[num])
        working_list.append(word)

    return working_list


# Load a set of words from the "dictionaries/" directory into lists.
def load_file(filename, characters, pinyin, translation):
    filename = 'dictionaries/' + filename.strip()

    if os.path.exists(filename):
        open_file = open(filename, 'r', encoding='utf8')

        for line in open_file:
            working_list = list(line.split(', '))
            characters.append(working_list[0].strip())
            pinyin.append(working_list[1].strip())
            translation.append(working_list[2].strip())

        open_file.close()
        new_pinyin = replace_tone(pinyin)

        for word in range(len(new_pinyin)):
            pinyin[word] = new_pinyin[word]

        load_status = True

    else:
        print('File not found.')

        load_status = False

    return load_status


# Randomly show each character and compare user input to the definition.
def test(filename, with_pinyin=''):
    if with_pinyin == '':
        with_pinyin = input('With pinyin? (y/n) ')

    characters = []
    pinyin = []
    translation = []

    if load_file(filename, characters, pinyin, translation):
        score = 0.0
        number_set = list(range(len(translation)))
        while len(number_set) > 0:
            question = random.randint(0, len(translation) - 1)
            while question not in number_set:
                question = random.randint(0, len(translation) - 1)

            if with_pinyin.strip() == 'y':
                print('Define:', characters[question], pinyin[question])
            else:
                print('Define:', characters[question])

            answer = ""

            while answer == "":
                answer = input()

            if '/' in translation[question]:
                correct = False
                multiple_translations = translation[question].split('/')
                for word in multiple_translations:
                    if answer.strip().lower() == word.lower():
                        correct = True
                if correct:
                    print('Right!')
                    score += 1
                else:
                    print('Wrong.')
            else:
                if answer.strip().lower() == translation[question].lower():
                    print('Right!')
                    score += 1
                else:
                    print('Wrong.')

            print()

            number_set.remove(question)

        print('Score:',
              str('%0.2f' % (score / len(translation) * 100)) + '%')


# Accept up to two arguments: the dictionary filename, and the option
# for displaying pinyin.
def main():
    if len(sys.argv) == 1:
        if os.path.exists('dictionaries'):
            print('Pick a subject:')
            dictionaries = os.listdir('dictionaries')
            dictionaries.sort()

            for dictionary in dictionaries:
                print(dictionary)

            test(input())
        else:
            print('Path "dictionaries" not found.')

    if len(sys.argv) == 2:
        argument = sys.argv[1]
        test(argument)

    if len(sys.argv) == 3:
        test(sys.argv[1], sys.argv[2])


main()
